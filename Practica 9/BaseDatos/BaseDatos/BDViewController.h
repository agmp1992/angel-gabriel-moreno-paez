//
//  BDViewController.h
//  BaseDatos
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "BDProxyBD.h"

@interface BDViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)oprimir:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong,nonatomic) BDProxyBD *proxyBD;

@end
