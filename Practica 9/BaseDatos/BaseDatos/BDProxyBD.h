//
//  BDProxyBD.h
//  BaseDatos
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDProxyBD : NSObject
-(void)insertarNombre:(NSString *)nombre;
-(NSMutableArray *)nombres;
@end
