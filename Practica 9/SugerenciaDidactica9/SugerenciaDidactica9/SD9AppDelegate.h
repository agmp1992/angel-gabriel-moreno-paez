//
//  SD9AppDelegate.h
//  SugerenciaDidactica9
//
//  Created by Angel Gabriel on 15/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD9AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
