//
//  SD9ViewController.h
//  SugerenciaDidactica9
//
//  Created by Angel Gabriel on 15/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "SD9ProxyBD.h"

@interface SD9ViewController : UIViewController
- (IBAction)eliminar:(id)sender;
- (IBAction)añadir:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *texto;
@property (strong,nonatomic) SD9ProxyBD *proxyBD;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@end
