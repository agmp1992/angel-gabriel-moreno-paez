//
//  SD9ViewController.m
//  SugerenciaDidactica9
//
//  Created by Angel Gabriel on 15/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD9ViewController.h"

@interface SD9ViewController ()

@end

@implementation SD9ViewController
@synthesize texto;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self copiaBD];
    self.proxyBD=[[SD9ProxyBD alloc]init];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.proxyBD nombres]count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text=[[self.proxyBD nombres]objectAtIndex:indexPath.row];
    return cell;
}

-(void)copiaBD{
    NSString* documentsPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES)objectAtIndex:0];
    NSString* bd=[documentsPath stringByAppendingPathComponent:@"mibd"];
    BOOL existeArchivo=[[NSFileManager defaultManager]fileExistsAtPath:bd];
    if(existeArchivo)return;
    NSString *rutaResource=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"mibd"];
    NSFileManager *administradorArchivos=[NSFileManager defaultManager];
    NSError *error=nil;
    
    if(![administradorArchivos copyItemAtPath:rutaResource toPath:bd error:&error]){
        UIAlertView *mensajeError=[[UIAlertView alloc]initWithTitle:@"¡Cuidado" message:@"No pude copiar la bd" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [mensajeError show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)añadir:(id)sender {
    [self.texto resignFirstResponder];
    [self.proxyBD insertarNombre:self.texto.text];
    [self.tabla reloadData];
}
- (IBAction)eliminar:(id)sender {
    [self.texto resignFirstResponder];
    [self.proxyBD eliminarNombre:self.texto.text];
    [self.tabla reloadData];
}
@end
