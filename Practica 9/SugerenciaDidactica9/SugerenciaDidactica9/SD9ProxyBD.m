//
//  SD9ProxyBD.m
//  SugerenciaDidactica9
//
//  Created by Angel Gabriel on 15/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD9ProxyBD.h"
#include <sqlite3.h>

@implementation SD9ProxyBD

-(NSMutableArray *)nombres{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta="Select nombre from informacion";
    NSString *ruta=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    sqlite3_open([ruta UTF8String], &laBd);
    sqlite3_prepare(laBd,consulta,-1,&consultaPreparada,NULL);
    NSMutableArray *resultados=[[NSMutableArray alloc]init];
    while (sqlite3_step(consultaPreparada)==SQLITE_ROW) {
        [resultados addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(consultaPreparada,0)]];
        
    }
    sqlite3_close(laBd);
    return resultados;
}

-(void)insertarNombre:(NSString *)nombre{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta="Insert into informacion(nombre) values(?)";
    NSString *ruta=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    sqlite3_open([ruta UTF8String], &laBd);
    sqlite3_prepare(laBd,consulta,-1,&consultaPreparada,NULL);
    sqlite3_bind_text(consultaPreparada,1,[nombre UTF8String],-1,SQLITE_TRANSIENT);
    sqlite3_step(consultaPreparada);
    sqlite3_finalize(consultaPreparada);
    sqlite3_close(laBd);
}

-(void)eliminarNombre:(NSString *)nombre{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta="Delete from informacion where nombre=?";
    NSString *ruta=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    sqlite3_open([ruta UTF8String], &laBd);
    sqlite3_prepare(laBd,consulta,-1,&consultaPreparada,NULL);
    sqlite3_bind_text(consultaPreparada,1,[nombre UTF8String],-1,SQLITE_TRANSIENT);
    sqlite3_step(consultaPreparada);
    sqlite3_finalize(consultaPreparada);
    int borrados=sqlite3_changes(laBd);
    if(borrados==0){
        UIAlertView * noelementoscoincidentes = [[UIAlertView alloc]initWithTitle:@"ERROR EN LA ELIMINACIÓN" message:@"El elemento que intentas borrar no existe" delegate:self cancelButtonTitle:@"ACEPTAR" otherButtonTitles:nil];
        [noelementoscoincidentes show];
    }
    else{
        NSString * mensaje=[NSString stringWithFormat:@"Se borraron %d elementos",borrados];
        UIAlertView *elementosborrados=[[UIAlertView alloc]initWithTitle:@"ELEMENTOS BORRADOS EXITOSAMENTE" message:mensaje delegate:self cancelButtonTitle:@"ACEPTAR" otherButtonTitles:nil];
        [elementosborrados show];
    }
    sqlite3_close(laBd);
}

@end
