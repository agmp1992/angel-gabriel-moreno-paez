//
//  SD9ProxyBD.h
//  SugerenciaDidactica9
//
//  Created by Angel Gabriel on 15/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SD9ProxyBD : NSObject
-(NSMutableArray *)nombres;
-(void)insertarNombre:(NSString *)nombre;
-(void)eliminarNombre:(NSString *)nombre;
@end
