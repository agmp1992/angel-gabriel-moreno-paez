//
//  main.m
//  SugerenciaDidactica13
//
//  Created by Angel Gabriel on 18/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SD13AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SD13AppDelegate class]));
    }
}
