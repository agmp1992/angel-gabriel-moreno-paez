//
//  SD13ViewController.m
//  SugerenciaDidactica13
//
//  Created by Angel Gabriel on 18/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD13ViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface SD13ViewController ()

@end

@implementation SD13ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)escribirCodigo:(id)sender {
    [self.texto resignFirstResponder];
    
    NSString *direccion=[NSString stringWithFormat:@"http://openweathermap.org/data/2.1/find/name?q=%s",[self.texto.text UTF8String]];
    NSString *direccionEscapada= [direccion stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url=[NSURL URLWithString:direccionEscapada];
    dispatch_async(kBgQueue, ^{
        NSData *data=[NSData dataWithContentsOfURL:
                      url];
        [self performSelectorOnMainThread:@selector(recogerDatos:) withObject:data waitUntilDone:YES];
    });
}

-(void)recogerDatos:(NSData *)responseData{
    
    NSError *error;
    NSDictionary *json=[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error ];
    
    NSMutableArray *parteInteresante=[json objectForKey:@"list"];
    if(parteInteresante==nil){
        UIAlertView *poblacionNoExistente=[[UIAlertView alloc]initWithTitle:@"POBLACIÓN NO ENCONTRADA" message:@"La población que buscas no existe en nuestra BD, favor de intentar con otra o verificar que los datos sean correctos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        self.temperatura.text=@"Temperatura";
        self.presion.text=@"Presión";
        self.poblacion.text=@"Población";
        [poblacionNoExistente show];
        
    }
    else{
        
        NSDictionary *temperaturas=[parteInteresante objectAtIndex:0];
        
        NSString *temperatura=[[temperaturas objectForKey:@"main"]objectForKey:@"temp"];
        NSString *presion=[[temperaturas objectForKey:@"main"]objectForKey:@"pressure"];
        NSString *poblacion=[[temperaturas objectForKey:@"sys"]objectForKey:@"population"];
        
        float t=[temperatura floatValue]-273;
        float p=[presion floatValue]/1000.0f;
        int po=[poblacion intValue];
        
        NSNumberFormatter *formateador=[[NSNumberFormatter alloc]init];
        [formateador setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *poblacionFormateada=[formateador stringFromNumber:[NSNumber numberWithInt:po]];
        
        self.temperatura.text=[NSString stringWithFormat:@"Temperatura %f ºC",t];
        self.presion.text=[NSString stringWithFormat:@"Presion es %f atmósferas",p];
        self.poblacion.text=[NSString stringWithFormat:@"Población son %@ habitantes",poblacionFormateada];
    }
}

@end
