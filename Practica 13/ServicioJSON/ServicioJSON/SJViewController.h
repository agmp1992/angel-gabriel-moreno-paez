//
//  SJViewController.h
//  ServicioJSON
//
//  Created by Angel Gabriel on 18/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJViewController : UIViewController
- (IBAction)escribirCodigo:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *texto;
@property (strong, nonatomic) IBOutlet UILabel *temperatura;
@property (strong, nonatomic) IBOutlet UILabel *presion;
@property (strong, nonatomic) IBOutlet UILabel *poblacion;

@end
