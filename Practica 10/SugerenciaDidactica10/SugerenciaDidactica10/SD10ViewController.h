//
//  SD10ViewController.h
//  SugerenciaDidactica10
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD10ViewController : UIViewController
- (IBAction)elegirSitio:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *navegador;
@property (strong, nonatomic) IBOutlet UIButton *googleButton;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *espnButton;
@property (strong, nonatomic) IBOutlet UIButton *bggButton;
@property (strong, nonatomic) IBOutlet UIButton *surpriseButton;
@property (strong, nonatomic) IBOutlet UILabel *googleLabel;
@property (strong, nonatomic) IBOutlet UILabel *facebookLabel;
@property (strong, nonatomic) IBOutlet UILabel *espnLabel;
@property (strong, nonatomic) IBOutlet UILabel *bggLabel;
@property (strong, nonatomic) IBOutlet UILabel *surpriseLabel;
@property (strong, nonatomic) IBOutlet UILabel *cabeceraLabel;

@end
