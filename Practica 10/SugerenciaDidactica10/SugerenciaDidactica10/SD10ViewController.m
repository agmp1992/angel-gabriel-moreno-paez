//
//  SD10ViewController.m
//  SugerenciaDidactica10
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//
#define Google 0
#define Facebook 1
#define ESPN 2
#define BoardGamesGeek 3
#define Surprise 4
#import "SD10ViewController.h"

@interface SD10ViewController (){
    NSString *direccion;
}
@end

@implementation SD10ViewController
@synthesize googleButton;
@synthesize googleLabel;
@synthesize facebookButton;
@synthesize facebookLabel;
@synthesize espnButton;
@synthesize espnLabel;
@synthesize bggButton;
@synthesize bggLabel;
@synthesize surpriseButton;
@synthesize surpriseLabel;
@synthesize cabeceraLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)elegirSitio:(id)sender {
    switch ([sender tag]) {
        case Google:
            direccion=@"https://www.google.com.mx";
            break;
        
        case Facebook:
            direccion=@"https://www.facebook.com";
            break;
            
        case ESPN:
            direccion=@"http://espndeportes.espn.go.com";
            break;
        
        case BoardGamesGeek:
            direccion=@"http://boardgamegeek.com";
            break;
        
        case Surprise:
            direccion=@"http://www.cruzazulfc.com";
            break;
    }
    NSURL *url=[NSURL URLWithString:direccion];
    NSURLRequest *peticion=[NSURLRequest requestWithURL:url];
    [self.navegador loadRequest:peticion];
    self.googleButton.hidden=YES;
    self.googleLabel.hidden=YES;
    self.facebookButton.hidden=YES;
    self.facebookLabel.hidden=YES;
    self.espnButton.hidden=YES;
    self.espnLabel.hidden=YES;
    self.bggButton.hidden=YES;
    self.bggLabel.hidden=YES;
    self.surpriseButton.hidden=YES;
    self.surpriseLabel.hidden=YES;
    self.cabeceraLabel.hidden=YES;
}
@end
