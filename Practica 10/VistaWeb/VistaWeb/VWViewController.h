//
//  VWViewController.h
//  VistaWeb
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VWViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *navegador;

@end
