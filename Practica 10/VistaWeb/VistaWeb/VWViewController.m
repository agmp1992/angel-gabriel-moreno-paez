//
//  VWViewController.m
//  VistaWeb
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "VWViewController.h"

@interface VWViewController ()

@end

@implementation VWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *direccion=@"http://google.com.mx";
    
    NSURL *url=[NSURL URLWithString:direccion];
    NSURLRequest *peticion=[NSURLRequest requestWithURL:url];
    [self.navegador loadRequest:peticion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
