//
//  MOTableViewController.h
//  EjemploModal
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MODetalleViewController.h"
@interface MOTableViewController : UITableViewController<AlumnoDetallesViewControllerDelegate>
@end
