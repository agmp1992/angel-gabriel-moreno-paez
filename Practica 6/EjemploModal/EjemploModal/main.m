    //
//  main.m
//  EjemploModal
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}
