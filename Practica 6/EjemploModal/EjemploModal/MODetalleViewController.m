//
//  MODetalleViewController.m
//  EjemploModal
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "MODetalleViewController.h"

@interface MODetalleViewController ()

@end

@implementation MODetalleViewController

@synthesize delegate;

-(IBAction)done:(id)sender{
    [self.delegate AlumnoDetallesViewControllerDelegateDidSave:self];
}
-(IBAction)cancel:(id)sender{
    [self.delegate AlumnoDetallesViewControllerDelegateDidCancel:self];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
