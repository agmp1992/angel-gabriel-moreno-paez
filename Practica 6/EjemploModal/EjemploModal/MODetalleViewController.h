//
//  MODetalleViewController.h
//  EjemploModal
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MODetalleViewController;
@protocol AlumnoDetallesViewControllerDelegate <NSObject>

-(void)AlumnoDetallesViewControllerDelegateDidSave:(MODetalleViewController *)controller;

-(void)AlumnoDetallesViewControllerDelegateDidCancel:(MODetalleViewController *)controller;


@end

@interface MODetalleViewController : UITableViewController

@property (nonatomic, strong)id <AlumnoDetallesViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *nombre;
-(IBAction)done:(id)sender;
-(IBAction)cancel:(id)sender;
@end
