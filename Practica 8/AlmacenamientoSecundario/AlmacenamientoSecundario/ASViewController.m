//
//  ASViewController.m
//  AlmacenamientoSecundario
//
//  Created by Angel Gabriel on 11/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "ASViewController.h"

@interface ASViewController ()

@end

@implementation ASViewController

-(NSString *)leerDatos{
    NSString *directorio = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES)objectAtIndex:0];
                            NSString *rutaCompleta=[directorio stringByAppendingPathComponent:@"archivo"];
                            NSStringEncoding codificacion= NSUTF8StringEncoding;
                            NSError *error;
                            NSString *textoLeido=[NSString stringWithContentsOfFile:rutaCompleta encoding:codificacion error:&error];
if(textoLeido==nil)
                            textoLeido=@"no inicializado";
                            return textoLeido;
          }
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.texto.text=[self leerDatos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)terminoescribir:(id)sender {
    [_texto resignFirstResponder];
}
    @end
