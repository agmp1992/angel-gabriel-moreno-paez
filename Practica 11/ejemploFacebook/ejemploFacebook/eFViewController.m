//
//  eFViewController.m
//  ejemploFacebook
//
//  Created by Angel Gabriel on 12/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "eFViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface eFViewController ()

@end

@implementation eFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
    SLComposeViewController *controladorSocial;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])//check if Facebook Account is linked
    {
        controladorSocial=[[SLComposeViewController alloc]init];
        controladorSocial=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him what social plattform to use it, e.g. facebook or twitter
        [controladorSocial setInitialText:@"sd"];
        [self presentViewController:controladorSocial animated:YES completion:nil];
    }
    [controladorSocial setCompletionHandler:^(SLComposeViewControllerResult result){
        NSString *output;
        switch(result){
            case SLComposeViewControllerResultCancelled:
                output=@"Cancelado";
            break;
            case SLComposeViewControllerResultDone:
                output=@"Trivia social posteada";
                break;
            default:
                break;
                
        }
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
    
}
@end
