//
//  SD5AppDelegate.h
//  SugerenciaDidactica5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD5AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
