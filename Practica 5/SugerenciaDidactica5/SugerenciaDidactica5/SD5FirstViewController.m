//
//  SD5FirstViewController.m
//  SugerenciaDidactica5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD5FirstViewController.h"

@interface SD5FirstViewController ()

@end

@implementation SD5FirstViewController

NSArray *ciudades;
int tipoDeCiudades=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    ciudades=[NSArray arrayWithObjects:
              [NSArray arrayWithObjects:@"Sinaloa",@"Los Mochis",@"Culiacán",@"Mazatlán",nil],
              [NSArray arrayWithObjects:@"Sonora",@"Obregón",@"Hermosillo",@"Nogales",nil],
              [NSArray arrayWithObjects:@"Chihuahua",@"Chihuahua",@"Parral",@"Ciudad Juárez",nil],
              nil];
}

-(void)viewDidUnload{
    ciudades=nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark pickerView

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;//indicamos que hay dos columnas en el pickerView
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component==0)
        return [ciudades count];//regresamos cuantos tipos de ciudades tenemos
    return [[ciudades objectAtIndex:tipoDeCiudades]count]-1;
    //regresamos cuantas ciudades de ese tipo tentemos restando el nombre del tipo
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component==0)//si cambian de seleccion de tipo
        if(tipoDeCiudades!=row){
            tipoDeCiudades=row;//guardamos el nuevo tipo
            [pickerView reloadComponent:1];
            //mandamos recargar las ciudades del tipo
        }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component==0)
        return [[ciudades objectAtIndex:row]objectAtIndex:0];
    return [[ciudades objectAtIndex:tipoDeCiudades]objectAtIndex:row+1];
}
@end
