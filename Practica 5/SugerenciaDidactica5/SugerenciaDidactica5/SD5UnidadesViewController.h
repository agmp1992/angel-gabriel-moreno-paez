//
//  SD5UnidadesViewController.h
//  SugerenciaDidactica5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD5UnidadesViewController : UITableViewController
    @property NSArray *unidades;
@end
