//
//  RA5FirstViewController.h
//  ReporteAlumno5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RA5FirstViewController : UIViewController
{
    NSTimer *timer;
}
-(void)actualizar;
@property (strong, nonatomic) IBOutlet UILabel *fechaactual;

@end
