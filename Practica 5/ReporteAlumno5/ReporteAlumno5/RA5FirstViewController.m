//
//  RA5FirstViewController.m
//  ReporteAlumno5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "RA5FirstViewController.h"

@interface RA5FirstViewController ()

@end

@implementation RA5FirstViewController

@synthesize fechaactual;

-(void)actualizar{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"MMM dd, yyyy HH:mm:ss"];
	NSDate *now=[[NSDate alloc]init];
    fechaactual.text=[dateFormat stringFromDate:now];}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    timer =[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(actualizar) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}


@end
