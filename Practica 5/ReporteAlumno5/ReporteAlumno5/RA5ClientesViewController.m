//
//  RA5ClientesViewController.m
//  ReporteAlumno5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "RA5ClientesViewController.h"
#import "RA5PedidosViewController.h"

@interface RA5ClientesViewController ()

@end

@implementation RA5ClientesViewController

NSArray *clientes;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier]isEqualToString:@"MuestraPedidos"]){
        NSIndexPath *selectedRowIndex=[self.tableView indexPathForSelectedRow];
   	    RA5PedidosViewController *detailViewController = [segue destinationViewController];
        NSMutableArray *fila=[[NSMutableArray alloc]init];
        detailViewController.pedidos=[[NSMutableArray alloc]init];

        for(int filas=1;filas<=[[clientes objectAtIndex:selectedRowIndex.row]count]-1;filas++){
            fila=[[NSMutableArray alloc]init];
            for(int elementos=0;elementos<=[[[clientes objectAtIndex:selectedRowIndex.row]objectAtIndex:filas]count]-1;elementos++){
                [fila addObject:[[[clientes objectAtIndex:selectedRowIndex.row] objectAtIndex:filas] objectAtIndex:elementos]];
            }
            [detailViewController.pedidos addObject:fila];
        }
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    clientes=
    [NSArray arrayWithObjects:
    
     [NSArray arrayWithObjects:
     @"Martin Martinez",
     [NSArray arrayWithObjects:@"000",@"Escoba",@"Mop",@"Manguera",nil],
     [NSArray arrayWithObjects:@"001",@"Catre",@"Jarcia",@"Estuco",nil],
     [NSArray arrayWithObjects:@"002",@"Mecate",@"Pala",@"Rastrillo",nil],
     nil],

     [NSArray arrayWithObjects:
      @"Rodrigo Rodríguez",
      [NSArray arrayWithObjects:@"100",@"Tornillo",@"Hamaca",@"Petate",nil],
      [NSArray arrayWithObjects:@"101",@"Jaula",@"Perro",@"Trampa",nil],
      [NSArray arrayWithObjects:@"102",@"Mop",@"Hacha",@"Talacho",nil],
      nil],

     [NSArray arrayWithObjects:
      @"Gonzalo González",
      [NSArray arrayWithObjects:@"200",@"Manguera",@"Contacto",@"Apagador",nil],
      [NSArray arrayWithObjects:@"201",@"Opresor",@"Alcayata",@"Amarrador",nil],
      [NSArray arrayWithObjects:@"202",@"Trapeador",@"Antena",@"Suadero",nil],
      nil],
     
     nil];
}

-(void)viewDidUnload{
    clientes=nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [clientes count];//Cuantos clientes tenemos en el arreglo
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cliente";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    cell.textLabel.text=[[clientes objectAtIndex:indexPath.row]objectAtIndex:0];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
