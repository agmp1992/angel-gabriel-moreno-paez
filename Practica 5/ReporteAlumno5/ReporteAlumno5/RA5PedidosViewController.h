//
//  RA5PedidosViewController.h
//  ReporteAlumno5
//
//  Created by Angel Gabriel on 08/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RA5PedidosViewController : UITableViewController
@property NSMutableArray *pedidos;
@end
