//
//  CAFirstViewController.h
//  ControlesAvanzados
//
//  Created by Angel Gabriel on 06/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAFirstViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource>

@end
