//
//  SWViewController.h
//  ServicioWeb
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWViewController : UIViewController <NSXMLParserDelegate>
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)terminarEscribir:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
@property (strong,nonatomic) NSMutableData *datosWeb;
@end
