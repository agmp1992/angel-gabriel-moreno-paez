//
//  SD12ViewController.m
//  SugerenciaDidactica12
//
//  Created by Angel Gabriel on 22/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD12ViewController.h"
#define CF 0
#define FC 1

@interface SD12ViewController (){
    NSMutableString *contenidoNodo;
    NSString *datosFinales;
}

@end

@implementation SD12ViewController

@synthesize datosWeb;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)conversion:(id)sender {
    contenidoNodo=[[NSMutableString alloc]init];
    
    [self.texto resignFirstResponder];
    
    if([sender tag]==1){

        NSString *formatoSoap= [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                                "<soap:Body>\n"
                                "<CelsiusToFahrenheit xmlns=\"http://www.w3schools.com/webservices/\">\n"
                                "<Celsius>%@</Celsius>\n"
                                "</CelsiusToFahrenheit>\n"
                                "</soap:Body>\n"
                                "</soap:Envelope>\n",self.texto.text];
        
        
        NSURL *direccionServicioWeb = [NSURL URLWithString:@"http://www.w3schools.com/webservices/tempconvert.asmx"];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc]initWithURL:direccionServicioWeb];
        
        NSString *longitudMensaje = [NSString stringWithFormat:@"%d",[formatoSoap length]];
        
        [peticion addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
        [peticion addValue:@"http://www.w3schools.com/webservices/CelsiusToFahrenheit" forHTTPHeaderField:@"SOAPAction"];
        [peticion addValue:longitudMensaje forHTTPHeaderField:@"Content-Length"];
        [peticion setHTTPMethod:@"POST"];
        //the below encoding is used to send data over the net
        [peticion setHTTPBody:[formatoSoap dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *conexion= [[NSURLConnection alloc]initWithRequest:peticion delegate:self];
        
        if (conexion) {
            datosWeb = [[NSMutableData alloc]init];
        }
        else {
            NSLog(@"No se estableció conexión");
        }
        
    }
    else if([sender tag]==0){
   
        NSString *formatoSoap= [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                                "<soap:Body>\n"
                                "<FahrenheitToCelsius xmlns=\"http://www.w3schools.com/webservices/\">\n"
                                "<Fahrenheit>%@</Fahrenheit>\n"
                                "</FahrenheitToCelsius>\n"
                                "</soap:Body>\n"
                                "</soap:Envelope>\n",self.texto.text];
        
        
        NSURL *direccionServicioWeb = [NSURL URLWithString:@"http://www.w3schools.com/webservices/tempconvert.asmx"];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc]initWithURL:direccionServicioWeb];
        
        NSString *longitudMensaje = [NSString stringWithFormat:@"%d",[formatoSoap length]];
        
        [peticion addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
        [peticion addValue:@"http://www.w3schools.com/webservices/FahrenheitToCelsius" forHTTPHeaderField:@"SOAPAction"];
        [peticion addValue:longitudMensaje forHTTPHeaderField:@"Content-Length"];
        [peticion setHTTPMethod:@"POST"];
        //the below encoding is used to send data over the net
        [peticion setHTTPBody:[formatoSoap dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *conexion= [[NSURLConnection alloc]initWithRequest:peticion delegate:self];
        
        if (conexion) {
            datosWeb = [[NSMutableData alloc]init];
        }
        else {
            NSLog(@"No se estableció conexión");
        }

    }
    

}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[datosWeb setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[datosWeb appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"Error en la conexión");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSXMLParser * xmlParser = [[NSXMLParser alloc]initWithData:datosWeb];
    
	// NSString *elXML = [[NSString alloc] initWithBytes: [datosWeb mutableBytes] length:[datosWeb length] encoding:NSUTF8StringEncoding];
    [xmlParser setDelegate:self];
	[xmlParser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	[contenidoNodo appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	if ([elementName isEqualToString:@"CelsiusToFahrenheitResult"]) {
		datosFinales=[NSString stringWithFormat:@"%@ º Fahrenheit",contenidoNodo];
    }
    else if([elementName isEqualToString:@"FahrenheitToCelsiusResult"]){
		datosFinales=[NSString stringWithFormat:@"%@ º Celsius",contenidoNodo];
    }
    self.etiqueta.text=datosFinales;
}
@end
