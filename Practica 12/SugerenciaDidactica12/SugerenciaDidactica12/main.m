//
//  main.m
//  SugerenciaDidactica12
//
//  Created by Angel Gabriel on 22/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SD12AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SD12AppDelegate class]));
    }
}
