//
//  SD12AppDelegate.h
//  SugerenciaDidactica12
//
//  Created by Angel Gabriel on 22/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD12AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
