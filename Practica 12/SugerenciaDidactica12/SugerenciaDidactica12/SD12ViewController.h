//
//  SD12ViewController.h
//  SugerenciaDidactica12
//
//  Created by Angel Gabriel on 22/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD12ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)conversion:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
@property (strong,nonatomic) NSMutableData *datosWeb;

@end
