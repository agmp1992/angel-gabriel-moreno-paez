//
//  SJViewController.h
//  SugerenciaDidacticaCalculadora
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJViewController : UIViewController
{
    int operando;
    double resultado;
    double numero1;
    double numero2;
    float decimal1;
    float decimal2;
    int contadorNumero;
    int numeroclickpunto;
    int numnumero;
    int decimal;
    int conteooperadores;
    BOOL puntoclick;
    BOOL operacionesActivadas;
    BOOL puntoActivado;
    BOOL igualactivado;
    BOOL fin;
}
@property (strong, nonatomic) IBOutlet UILabel *    display;
- (IBAction)inversoaditivo:(id)sender;
- (IBAction)clearScreen:(id)sender;
- (IBAction)numero:(id)sender;
- (IBAction)punto:(id)sender;
- (IBAction)igual:(id)sender;
- (IBAction)operacion:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *signo;

@end
