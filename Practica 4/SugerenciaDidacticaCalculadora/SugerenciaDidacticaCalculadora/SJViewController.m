//
//  SJViewController.m
//  SugerenciaDidacticaCalculadora
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#define DIVISION 0
#define MULTIPLICACION 1
#define SUMA 2
#define RESTA 3

#import "SJViewController.h"

@interface SJViewController ()

@end

@implementation SJViewController

@synthesize display;
@synthesize signo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    operando=10;
    contadorNumero=0;
    numeroclickpunto=0;
    numnumero=0;
    operacionesActivadas=false;
    puntoclick=false;
    fin=false;
    numero1=0;
    numero2=0;
    decimal1=0;
    decimal2=0;
    decimal=0;
    resultado=0;
    conteooperadores=0;
    display.text=@"";
    signo.text=@"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)inversoaditivo:(id)sender {
    if(operando==10){
        numero1=numero1*-1;
        display.text=[NSString stringWithFormat:@"%f",numero1];
    }
    else{
        if(fin==false){
            numero2=numero2*-1;
            display.text=[NSString stringWithFormat:@"%f",numero2];
        }
        else{
            resultado=resultado*-1;
            display.text=[NSString stringWithFormat:@"%f",resultado];
        }
    }
}

- (IBAction)clearScreen:(id)sender {
    [self viewDidLoad];
}

- (IBAction)numero:(id)sender {
    if(fin==false){
        if(numeroclickpunto==0 || numeroclickpunto==2){
            numeroclickpunto++;
        }
        if(operando==10){
            if(puntoclick==false){
                if(contadorNumero==0){
                    numero1=[sender tag];
                    contadorNumero++;
                }
                else{
                    numero1=numero1*10+[sender tag];
                }
            }
            else{
                decimal++;
                numero1=numero1+([sender tag]/(pow(10,decimal)));
                NSLog(@"%f",numero1);
                
            }
            display.text=[NSString stringWithFormat:@"%f",numero1];
            
        }
        else{
            if(puntoclick==false){
                NSLog(@"%f",numero2);
                if(contadorNumero==0){
                    numero2=[sender tag];
                    contadorNumero++;
                    NSLog(@"%f",numero2);
                    
                }
                else{
                    numero2=numero2*10+[sender tag];
                    NSLog(@"%f",numero2);
                }
            }
            else{
                decimal++;
                numero2=numero2+([sender tag]/(pow(10,decimal)));
                NSLog(@"%f",numero2);
                
            }
            display.text=[NSString stringWithFormat:@"%f",numero2];
        }
        if(conteooperadores==0){
            conteooperadores++;
            operacionesActivadas=true;
        }
        
    }
}

- (IBAction)punto:(id)sender {
    if(fin==false){
        if(numeroclickpunto==1||numeroclickpunto==3){
            puntoclick=true;
        }
     }
}

- (IBAction)igual:(id)sender {
    if(fin==false){
        if(operando==10){
            NSString * algo=[NSString stringWithFormat:@"%f",numero1];
            display.text=algo;
        }
        else{
            switch (operando) {
                case SUMA:
                    resultado=numero1+numero2;
                    break;
                case RESTA:
                    resultado=numero1-numero2;
                    break;
                case MULTIPLICACION:
                    resultado=numero1*numero2;
                    break;
                case DIVISION:
                    if(numero2==0){
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"No puedes dividir entre cero" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                    else {
                        resultado=numero1/numero2;
                    }
                    break;
            }
            NSString *algo=[NSString stringWithFormat:@"%f",(resultado)];
            display.text=algo;
        }
        signo.text=@"";
        fin=true;
        
    }
}

- (IBAction)operacion:(id)sender {
    if(fin==false){
        if(operacionesActivadas==true){
            igualactivado=false;
            operacionesActivadas=false;
            numeroclickpunto++;
            operando=[sender tag];
            decimal=0;
            contadorNumero=0;
            puntoclick=false;
            switch ([sender tag]) {
                case SUMA:
                    signo.text=@"+";
                    break;
                case RESTA:
                    signo.text=@"-";
                    break;
                case MULTIPLICACION:
                    signo.text=@"*";
                    break;
                case DIVISION:
                    signo.text=@"/";
                    break;
            }
        }
  
    }
}

@end
