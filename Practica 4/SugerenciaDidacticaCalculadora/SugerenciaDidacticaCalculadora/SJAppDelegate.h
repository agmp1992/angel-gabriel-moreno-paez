//
//  SJAppDelegate.h
//  SugerenciaDidacticaCalculadora
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
