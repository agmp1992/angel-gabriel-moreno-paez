//
//  main.m/Users/maciosdeveloping/Desktop/SugerenciaDidacticaCalculadora/SugerenciaDidacticaCalculadora/SJViewController.m
//  SugerenciaDidacticaCalculadora
//
//  Created by Angel Gabriel on 13/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SJAppDelegate class]));
    }
}
