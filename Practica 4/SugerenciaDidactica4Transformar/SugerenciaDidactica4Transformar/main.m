//
//  main.m
//  SugerenciaDidactica4Transformar
//
//  Created by Angel Gabriel on 09/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SD4TAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SD4TAppDelegate class]));
    }
}
