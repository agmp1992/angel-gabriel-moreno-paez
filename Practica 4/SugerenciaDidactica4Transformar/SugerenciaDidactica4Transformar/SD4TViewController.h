//
//  SD4TViewController.h
//  SugerenciaDidactica4Transformar
//
//  Created by Angel Gabriel on 09/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SD4TViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
- (IBAction)cambioInfo:(id)sender;
- (IBAction)terminaEdicion:(id)sender;

@end
