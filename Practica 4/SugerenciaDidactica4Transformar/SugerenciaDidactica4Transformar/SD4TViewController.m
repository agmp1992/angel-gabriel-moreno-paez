//
//  SD4TViewController.m
//  SugerenciaDidactica4Transformar
//
//  Created by Angel Gabriel on 09/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import "SD4TViewController.h"

@interface SD4TViewController ()

@end

@implementation SD4TViewController
@synthesize texto;
@synthesize etiqueta;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cambioInfo:(id)sender {
    NSUInteger longitud=[texto.text length];
    NSMutableString *rtr=[NSMutableString stringWithCapacity:longitud];
    //rtr=texto.txt;
    while(longitud>(NSUInteger)0){
        unichar uch=[texto.text characterAtIndex:--longitud];
        [rtr appendString:[NSString stringWithCharacters:&uch length:1]];
    }
    etiqueta.text=rtr;
}

- (IBAction)terminaEdicion:(id)sender {
    [texto resignFirstResponder];
}
@end
